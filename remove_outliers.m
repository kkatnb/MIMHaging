function [cldata]=remove_outliers(raw_data,olsids)
% this function cleans up data set from out liers
% olsids=indices of outliers
% cldata: clean data with out any out liers
blankset=[];
id=1;
for i=1:length(olsids)
    tmp=raw_data(id:olsids(i)-1,:);
    id=olsids(i)+1;
    blankset=[blankset;tmp];
end
%last segment
tmp=raw_data(id:end,:);
blankset=[blankset;tmp];
cldata=blankset;
end
    