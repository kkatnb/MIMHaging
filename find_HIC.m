function [labels]=find_HIC(inpdata,F,par, olsids,k)
% this function specify the hierarchical clustering for cuts from 2..5
Y=pdist(inpdata,'euclid');
z=linkage(Y,par);
figure;
dendrogram(z,79)
if strcmp(par,'ward')
   ylim([0 10]);
else
   ylim([0 6]);
end
if F
    title(['Hierarchical clustering for the data after removing ouliers',num2str(olsids'),' using ',par,' linkage']);
else
    title(['Hierarchical clustering for the data before removing any outliers using ',par,' linkage']);
end
c=2:1:k;
% c=zeros(1,k-1);
% for i=1:length(c)
%     c(i)=i+1;
% end
    
T = cluster(z,'maxclust',c);
labels=T;
end
