function CH=find_CHI(inpdata,clusters,Nc,cntrs)
%this function provide the Calinski-Harabasz index CH
%inputs :
%1- the whole dataset inpdata
%2-clusters (elements inside them)
%3- Nc: # number of clusters
%4- 1xNc array stores the centroids of each cluster
d_center=mean(inpdata); % center of the data
[n,w]=size(inpdata);
SH=0;
dc=0;
numerator=0;
for i=1:Nc
    den=0;%accumulator for the denumenator
    if w>1
      x1=cntrs(i,:);
    else
        x1=cntrs(i);% this is just in cases when we have single feature
    end
    for j=1:length(clusters{i})
        if w>1
           x=inpdata(clusters{i}(j),:);%data sample
        else
           x=inpdata(clusters{i}(j));% this is just in cases when we have single feature
        end
        
        di=find_distance(x,x1);
        den=den+di;
    end
    
    dc=dc+length(clusters{i})* find_distance(x1,d_center);
    numerator=numerator+den;
%     SH=SH+numerator/den;
end
SH=dc/numerator;
CH=SH*(n-Nc)/(Nc-1);
end
        
        
