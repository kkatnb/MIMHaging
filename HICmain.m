% clc;
% clear;
opt2=' ';
while ~strcmp(opt2,'y') && ~strcmp(opt2,'n')
    opt2=input('Are you removing outliers this time? [enter: y or n]:','s');
end
%%****** uncomment the following 2 lines if you are reading from excel or trying to remove anyoutliers
% after removing outliers comment these two lines and load the filterred
% data manualy into Matlab WS
inpdset1=xlsread('S:\khalid\MIMH\newcleanrun\inputdata\imaging_combined_filterred_normalizedftrs_namesincluded.xlsx');
inpdset=inpdset1(:,2:end);%>>>>>>>>>>>>>>>>>> here use this line for full
%************
% data without removing any samples
if strcmp(opt2,'y')
    inpdset=inpdset1(:,2:end);
else
    msgbox('Pls, load the variable that contains the data set you want to cluster and assign it to the variable inpdset', 'Important');
end 
outliers=[];
option=input('Pls, enter HIC or k:','s');
if strcmp(option,'HIC')
    parameter=' ';
    while ~ strcmp(parameter,'complete')&& ~strcmp(parameter,'ward') && ~strcmp(parameter,'median')
        parameter=input('Pls, enter the linkage tool paramete:[complete, ward, or median]','s');
    end
    cmax=' ';
    while ~isnumeric(cmax)|| cmax<2
        cmax=input('pls, enter the max number of cutlevels you want to see (i.e. 10):');
    end
    cl=cmax;
    labels=find_HIC(inpdset,0,parameter,outliers,cmax);
    if strcmp(opt2,'y')
       fname=[parameter,num2str(cmax),'levelsolsremoved.xlsx'];% change name
    else
       fname=[parameter,num2str(cmax),'levelsols_not_removed.xlsx'];
    end
else
    kmns=' ';
    while ~ strcmp(kmns,'kmeans')&& ~strcmp(kmns,'kmedoids')
        kmns=input('Pls, chose kmeans or kmedoids:','s');
    end
    
    k=' ';
    while ~isnumeric(k)|| k<2
        k=input('pls, enter the max number of clusters:');
    end
    cl=k;
    labels=implement_kmeans(inpdset,kmns,k);
    if strcmp(opt2,'y')
       fname=[kmns,num2str(k),'levelsolsremoved.xlsx'];% change name
    else
       fname=[kmns,num2str(k),'levelsols_removed.xlsx'];% or me
    end
end
%% evaluation using internal indices
outliersids=[];
ftrs=size(inpdset,2);
cutinglvls=size(labels,2);
vinds=zeros(3,cutinglvls);


for i=1:cutinglvls
    tmp=labels(:,i);
    if strcmp(opt2,'y')
        [olsid,o_inds]=find_ols(tmp);% get an array that has the outliers cluster indices
        
        if ~isempty(o_inds)
            inpdset=remove_outliers(inpdset,o_inds);
            inpdset1=remove_outliers(inpdset1,o_inds);
            outliers=print_outliers_true_ids(outliers,o_inds,i+1);
            if strcmp(option,'HIC')
                labels=find_HIC(inpdset,1,parameter, outliers,cmax);
            else
                labels=implement_kmeans(inpdset,kmns,k);
            end
            tmp=labels(:,i);
        end
    end
    clsind=unique(tmp);
    Nc=length(clsind);
    clusters=cell(Nc,1);
    centroids=zeros(Nc,ftrs);
    for j=1:Nc
        clusters{j}= find(tmp==clsind(j));
        tmpc=inpdset(clusters{j});
        centroids(j,:)=mean(tmpc);
    end
    DBi=find_DBI(inpdset,centroids,clusters,Nc);
    vinds(1,i)=DBi;% davis boulding index
    SI=find_SI(Nc,clusters,inpdset,tmp);
    vinds(2,i)=SI;% sillhouete index
    CHI=find_CHI(inpdset,clusters,Nc,centroids);
    vinds(3,i)=CHI;
    
end
final=[labels;vinds];
tmplast=[inpdset1(:,1);0;0;0];
final=[tmplast,final];
% fname=['output_',parameter,'_linkage.xlsx'];
xlswrite(['S:\khalid\MIMH\newcleanrun\outputs\combined\',fname],final);

n=size(labels,1);
%% cvap preperation
sfile=labels;
tmp=2:cl;
sfile=[tmp;sfile];
fnametxt=[fname(1:end-4),'txt'];
% don't forget to change the path before you save your file
dlmwrite(['S:\khalid\MIMH\newcleanrun\outputs\combined\',fnametxt],sfile,'delimiter','\t','precision',6);
% these two lines uncomment them only once during the first data
% preperation after that they are unnecessary
% inpc=inpdset;
% dlmwrite('S:\khalid\MIMH\newcleanrun\outputs\combined\input_uncombined_ols_removed.txt',inpc,'delimiter','\t','precision',6);
% 




